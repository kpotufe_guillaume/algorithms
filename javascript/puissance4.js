/*
Created on 24 febr. 2014;
@author: ylmkpotufe;
*/

var width   = 7;
var height  = 6;
var winlen  = 4;
var tsize   = width*height;
var visuals = ['#','X','.'];
var timeout = 20;
var signs   = [-1,1];
var states  = ['WIN','PLAYING','DRAW'];
var depth   = 2;
var e       = null;

var MapConfig = function() {
  //""" A map configuration for(p4 """;
  this.pmap       = [];
  this.colHeads   = [0];
  this.turns      = [];
  //0=terminated, 1=running, 2=pat, 3=pause;
  this.state      = 1;
  this.winner     = 2;
  this.winpos     = -1;
  this.winor      = '*';
  this.players    = ["Player_#","Player_X","No winner"];
  this.heuristic  = 0;
  this.heurMap    = [0];
  for(var i=0;i<tsize;i++) {
    this.pmap.push(2);
    this.heurMap.push(0);
  }
  for(var i=0;i<width;i++){
    this.colHeads.push(0);
  }
}

function scoreUpHelper(wcoef,hcoef,spos,_map){
    res     = [];
    orient  = wcoef*width+hcoef;
    tempos  = spos;
    while( tempos+orient >= 0 && tempos+orient<tsize && (tempos%width+hcoef)>=0 && (tempos%width+hcoef)<width && _map[tempos] == _map[tempos+orient]){
        res.push(tempos+orient);
        tempos = tempos+orient;
    }
    //print("res: %s<br/>",[res]);
    return res;
}
function scoreUp(wcoef,hcoef,spos,_map){
   return len(scoreUpHelper(wcoef,hcoef,spos,_map))+len(scoreUpHelper(-wcoef,-hcoef,spos,_map))+1;
}

function evalUpHelper(spos,_map,_heurMap,_player,wcoef,hcoef){
    p1  = scoreUpHelper(wcoef,hcoef,spos,_map);
    p1  = p1.concat(scoreUpHelper(-wcoef,-hcoef,spos,_map));
    p1.push(spos);
    pw  = len(p1);
    pw  = pw + Math.floor(pw/4)*5;
    //print("p1=%s; ",[p1]);
    for( elm in p1) {
        //_heurMap[elm] = Math.max(Math.abs(_heurMap[elm]),(10**pw))*signs[_player];
        _heurMap[p1[elm]] += Math.pow(10,pw)*signs[_player];
    }
    return _heurMap;
}

function setHeuristic(_mapConfig){
    h = 0;
    for( var i=0;i<tsize;i++){
        h += _mapConfig.heurMap[i];
    }
    _mapConfig.heuristic = h;
    return h;
}

function evalUp(_mapConfig,_col){
    spos          = _mapConfig.colHeads[_col]*width+_col;
    spos,_player  =len(_mapConfig.turns)%2;
    //print("map tempos: %s<br/>",[_mapConfig.pmap[spos]]);
//    print( _mapConfig.heurMap,'---<br/>');
    _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,0,1);
//    print( _mapConfig.heurMap,'---<br/>');
    _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,1,0);
//    print( _mapConfig.heurMap,'---<br/>');
    _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,1,-1);
//    print( _mapConfig.heurMap,'---<br/>');
    _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,1,1);
//    print( _mapConfig.heurMap,_player,'---');
    setHeuristic(_mapConfig);
}

function isTerminal(node){
    return (len(node.turns) == tsize);
}

function heuristic(node){
    return node.heuristic;
}

function possibleColumns(node){
  var pc = [];
  for(var i=0;i<width;i++) if(node.colHeads[i] < height) pc.push(i);
  return pc;
}

function fillAndCheckWin(_mapConfig,_col,_nextplayer){
    pos                         = _mapConfig.colHeads[_col]*width+_col;
    _mapConfig.pmap[pos]        = _nextplayer;
    if(scoreUp(0,1,pos,_mapConfig.pmap) >= winlen){
        return [1,pos,'--'];
    }
    if(scoreUp(1,0,pos,_mapConfig.pmap) >= winlen){
        return [1,pos,'|'];
    }
    if(scoreUp(1,-1,pos,_mapConfig.pmap) >= winlen){
        return [1,pos,'\\'];
    }
    if(scoreUp(1,1,pos,_mapConfig.pmap) >= winlen){
        return [1,pos,'/'];
    }
    return [0,0,'*'];
}

function addTempColumn(_mapConfig, _col){
    lt                          = len(_mapConfig.turns);
    _nextplayer                 = lt%2;
    //Sanity check avoided;
    pos                         = _mapConfig.colHeads[_col]*width+_col;
    _mapConfig.pmap[pos]        = _nextplayer;
    evalUp(_mapConfig,_col);
    _mapConfig.colHeads[_col]  += 1;
    _mapConfig.turns.push(_col);
    return _mapConfig;
}

function resetTempColumn(_mapConfig, _col){
    _mapConfig.turns.pop();
    _mapConfig.colHeads[_col]  -= 1;
    lt                          = len(_mapConfig.turns);
    _nextplayer                 = lt%2;
    pos                         = _mapConfig.colHeads[_col]*width+_col;
    _mapConfig.pmap[pos]        = 2;
    return _mapConfig;
}

function alphabeta(node, depth, alpha, beta, maximizingPlayer){
    bestcolumn    = -1;
    if( depth == 0 || isTerminal(node)) { return [heuristic(node), bestcolumn]; }
    var pcs       = possibleColumns(node);
    if( maximizingPlayer){        
        for( k in pcs){
            var column  = pcs[k]; 
            tmpHeurMap  = node.heurMap.slice(0);
            tmpHeur     = node.heuristic;
            addTempColumn(node,column);
            //print("Node: %s<br/>",[node.turns]);
            score           = alphabeta(node, depth - 1, alpha, beta, false)[0];
            if(score > alpha){
                alpha       = score;
                bestcolumn  = column;
            }
            resetTempColumn(node,column);
            node.heurMap    = tmpHeurMap;
            node.heuristic  = tmpHeur;
            if( beta <= alpha){ break; }
        }
        return [alpha, bestcolumn];
    }
    else{
        for( k in pcs){
            var column  = pcs[k]; 
            tmpHeurMap  = node.heurMap.slice(0);
            tmpHeur     = node.heuristic;
            addTempColumn(node,column);
            //print("Node2: %s",[node.turns]);
            score           = alphabeta(node, depth - 1, alpha, beta, true)[0];
            if( score < beta){
                beta        = score;
                bestcolumn  = column;
            }
            resetTempColumn(node,column);
            node.heurMap    = tmpHeurMap;
            node.heuristic  = tmpHeur;
            if( beta <= alpha){ break; }
        }
        return [beta, bestcolumn];
    }
}

function pepetoMove(node){
    print("H=%s; Pepeto would play: %s",[setHeuristic(node),alphabeta(node, 2, -Infinity, Infinity, false)]);
}

function printMap(_map){
    //os.system('cls');
    /*try{
      document.body.remove();}
    catch(e){
      console.log(e);
    }*/
    e.innerHTML="";
    print("<br/>----------- BEGIN MAP ----------------");
    iprint("    &nbsp;&nbsp;&nbsp;&nbsp;");
    for( var i=0;i<width;i++){
        iprint(" &nbsp;&nbsp; &nbsp;"+[i]);
    }
    print(' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      ');
    for(var j=height-1;j>=0;j-- ){
        iprint(" &nbsp;&nbsp; "+[j]);
        for(var i=0;i<width;i++){
            iprint(' &nbsp;&nbsp;'+visuals[_map[j*width + i]]+' &nbsp;&nbsp;');
        }
        print('       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
    }
    print("----------- END  MAP  ----------------");
}

function main(){
    mapConfig               = new MapConfig();
    newHtmlElement();
    print("*** Puissance 4  ***<br/>/by Yawo Kpotufe, mcguy2008_NOSPAM_gmail/. <br/>You can play against Pepeto the master, or with a friend.<br/><br/>");

    newHtmlElement();
    mapConfig.players[0]    = raw_input("1st (//) player\'s name (Enter Pepeto to play against me ): ");
    mapConfig.players[1]    = raw_input('2nd (X) player\'s name: ');
    if( mapConfig.players[0] == "Pepeto"){
        depth = parseInt(raw_input("Pepeto's power level (1-7): "));
    }
    while(mapConfig.state == 1){
        lenturns    = len(mapConfig.turns);
        nextplayer  = lenturns%2 //return 0 || 1. 0 always start the game;
        //GET the user play. The colum is sufficient.;
        if( mapConfig.players[nextplayer] == "Pepeto"){
            //pepetoMove(mapConfig);
            column = alphabeta(mapConfig, depth, -Infinity, Infinity, false)[1];
            print("<br/>Pepeto played %s",[column]);
        }
        else{
            printMap(mapConfig.pmap);
            column = parseInt(raw_input('%s\'s move (100=pass, 200=abandon) : ',[mapConfig.players[nextplayer]]));
            while(column < 0 || column >= width || mapConfig.colHeads[column] >= height ){
                column = parseInt(raw_input('BAD MOVE ! %s\'s move (100=pass, 200=abandon) : ',[mapConfig.players[nextplayer]]));
            }
        }
        //printMap(mapConfig.pmap);
        if(column == 200){
          mapConfig.winner  = 1-nextplayer;
          mapConfig.state   = 0;
        }
        else{
            var res           = fillAndCheckWin(mapConfig,column,nextplayer);
            facw              = res[0];
            mapConfig.winpos  = res[1];
            mapConfig.winor   = res[2];
            if(facw == 1){
              mapConfig.winner  = nextplayer;
              mapConfig.state   = 0;
            }
            else if(lenturns == tsize - 1){
                mapConfig.state   = 2;
            }
            evalUp(mapConfig,column);
            //print("after evalUp: %s",[mapConfig.heurMap]);
            mapConfig.colHeads[column] += 1;
            mapConfig.turns.push(column);
        }
    }        
    printMap(mapConfig.pmap);
    print("<br/>###################################<br/>"+
    "Winner       : %s<br/>"+
    "Total turns  : %s<br/>"+
    "Final state  : %s<br/>"+
    "WinPos       : %s:%s<br/>"+
    "Orientation  : %s<br/>"+
    "TurnsHistory : %s",[mapConfig.players[mapConfig.winner],len(mapConfig.turns),states[mapConfig.state],mapConfig.winpos%width,mapConfig.winpos/width,mapConfig.winor,mapConfig.turns]);
}

function len(arr){
  return arr.length;
}

function print(){
  e.insertAdjacentHTML('beforeEnd',Array.prototype.slice.call(arguments));
  e.insertAdjacentHTML('beforeEnd',"<br/>");
} 
function iprint(){
  e.insertAdjacentHTML('beforeEnd',Array.prototype.slice.call(arguments));
} 
function raw_input(){
  return prompt(Array.prototype.slice.call(arguments).join(", "));
}

function newHtmlElement(){
  if(document.body==null)document.appendChild(document.createElement("body"));
  e=document.body.appendChild(document.createElement("div"));
}

