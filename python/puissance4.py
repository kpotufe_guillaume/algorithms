'''
Created on 24 febr. 2014
@author: ylmkpotufe
'''
#from itertools import *

width   = 7
height  = 6
winlen  = 4
tsize   = width*height
visuals = ['#','X','.']
timeout = 20
signs   = [-1,1]
states  = ['WIN','PLAYING','DRAW']
depth   = 2
class MapConfig:
    """ A map configuration for p4 """
    def __init__(self):
        self.pmap       = [2]*(tsize)
        self.colHeads   = [0]*(width)
        self.turns      = [] 
        #0=terminated, 1=running, 2=pat, 3=pause
        self.state      = 1
        self.winner     = 2
        self.winpos     = -1
        self.winor      = '*'
        self.players    = ["Player_#","Player_X","No winner"]     
        self.heuristic  = 0   
        self.heurMap    = [0]*(tsize)
        
def scoreUpHelper(wcoef,hcoef,spos,_map):
    res     = []    
    orient  = wcoef*width+hcoef
    tempos  = spos
    while tempos+orient >= 0 and tempos+orient<tsize and (tempos%width+hcoef)>=0 and (tempos%width+hcoef)<width and _map[tempos] == _map[tempos+orient]:
        res.append(tempos+orient)
        tempos = tempos+orient
    #print "res: %s\n"%res
    return res

def scoreUp(wcoef,hcoef,spos,_map):
   return len(scoreUpHelper(wcoef,hcoef,spos,_map))+len(scoreUpHelper(-wcoef,-hcoef,spos,_map))+1

def evalUpHelper(spos,_map,_heurMap,_player,wcoef,hcoef):
    p1  = scoreUpHelper(wcoef,hcoef,spos,_map)
    p1.extend(scoreUpHelper(-wcoef,-hcoef,spos,_map))
    p1.append(spos)
    pw  = len(p1)
    pw  = pw + int(pw/4)*5
    #print "p1=%s; "%p1
    for elm in p1 :
        #_heurMap[elm] = max(abs(_heurMap[elm]),(10**pw))*signs[_player]
        _heurMap[elm] += (10**pw)*signs[_player]
    return _heurMap

def setHeuristic(_mapConfig):
    h = 0
    for i in range(tsize):
        h += _mapConfig.heurMap[i]
    _mapConfig.heuristic = h
    return h


def evalUp(_mapConfig,_col):
    spos,_player        = _mapConfig.colHeads[_col]*width+_col,len(_mapConfig.turns)%2
    #print "map tempos: %s\n"%_mapConfig.pmap[spos]
#    print  _mapConfig.heurMap,'---\n'
    _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,0,1)
#    print  _mapConfig.heurMap,'---\n'
    _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,1,0)
#    print  _mapConfig.heurMap,'---\n'
    _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,1,-1)
#    print  _mapConfig.heurMap,'---\n'
    _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,1,1)
#    print  _mapConfig.heurMap,_player,'---'
    setHeuristic(_mapConfig)

def isTerminal(node):
    return (len(node.turns) == tsize)
 
def heuristic(node):
    return node.heuristic

def possibleColumns(node):
    return [i for i in range(width) if node.colHeads[i] < height]

def fillAndCheckWin(_mapConfig,_col,_nextplayer):
    pos                         = _mapConfig.colHeads[_col]*width+_col
    _mapConfig.pmap[pos]        = _nextplayer
    if(scoreUp(0,1,pos,_mapConfig.pmap) >= winlen):                    
        return 1,pos,'--'
    if(scoreUp(1,0,pos,_mapConfig.pmap) >= winlen):
        return 1,pos,'|'
    if(scoreUp(1,-1,pos,_mapConfig.pmap) >= winlen):
        return 1,pos,'\\'
    if(scoreUp(1,1,pos,_mapConfig.pmap) >= winlen):
        return 1,pos,'/'    
    return 0,0,'*'
    
def addTempColumn(_mapConfig, _col):
    lt                          = len(_mapConfig.turns)
    _nextplayer                 = lt%2
    #Sanity check avoided
    pos                         = _mapConfig.colHeads[_col]*width+_col
    _mapConfig.pmap[pos]        = _nextplayer
    evalUp(_mapConfig,_col)
    _mapConfig.colHeads[_col]  += 1
    _mapConfig.turns.append(_col)
    return _mapConfig
    
def resetTempColumn(_mapConfig, _col):
    _mapConfig.turns.pop()
    _mapConfig.colHeads[_col]  -= 1
    lt                          = len(_mapConfig.turns)
    _nextplayer                 = lt%2
    pos                         = _mapConfig.colHeads[_col]*width+_col
    _mapConfig.pmap[pos]        = 2 
    return _mapConfig
    

def alphabeta(node, depth, alpha, beta, maximizingPlayer):
    bestcolumn      = -1
    if depth == 0 or isTerminal(node) :
        return heuristic(node), bestcolumn
    if maximizingPlayer:        
        for column in possibleColumns(node):
            tmpHeurMap  = list(node.heurMap)
            tmpHeur     = node.heuristic
            addTempColumn(node,column)     
            #print "Node: %s\n"%node.turns
            score           = alphabeta(node, depth - 1, alpha, beta, False)[0]    
            if(score > alpha):
                alpha       = score   
                bestcolumn  = column                        
    
            resetTempColumn(node,column)
            node.heurMap    = tmpHeurMap
            node.heuristic  = tmpHeur     
            if beta <= alpha:
                break 
            
        return alpha, bestcolumn
    else:
        for column in possibleColumns(node):
            tmpHeurMap  = list(node.heurMap)
            tmpHeur     = node.heuristic
            addTempColumn(node,column)    
            #print "Node2: %s"%node.turns
            score           = alphabeta(node, depth - 1, alpha, beta, True)[0]
            if( score < beta):
                beta        = score
                bestcolumn  = column 
            
            resetTempColumn(node,column)
            node.heurMap    = tmpHeurMap 
            node.heuristic  = tmpHeur     
            if beta <= alpha:
                break 
        return beta, bestcolumn

def pepetoMove(node):
    print "H=%s; Pepeto would play: %s"%(setHeuristic(node),alphabeta(node, 2, float("-inf"), float("inf"), False))    


def printMap(_map):
    #os.system('cls')
    print "\n----------- BEGIN MAP ----------------"
    print "    ",
    for i in range(width):        
        print " %.2i  "%i,
    print '       '
    for j in reversed(range(height)):
        print " %.2i "%j,
        for i in range(width):
            print ' ',visuals[_map[j*width + i]],' ',
        print '       ' 
    print "----------- END  MAP  ----------------"

if __name__ == '__main__':
    mapConfig               = MapConfig()
    print "*** Puissance 4  ***\n/by Yawo Kpotufe, mcguy2008_NOSPAM_gmail/. \nYou can play against Pepeto, the master or with a friend.\n\n"
    mapConfig.players[0]    = raw_input("1st (#) player\'s name (Enter Pepeto to play against me ): ")
    mapConfig.players[1]    = raw_input('2nd (X) player\'s name: ')
    if( mapConfig.players[0] == "Pepeto"):
        depth = int(raw_input("Pepeto's power level (1-7): "))
    while(mapConfig.state == 1):
        lenturns    = len(mapConfig.turns)        
        nextplayer  = lenturns%2 #return 0 or 1. 0 always start the game
        #GET the user play. The colum is sufficient.
        if( mapConfig.players[nextplayer] == "Pepeto"):
            #pepetoMove(mapConfig)
            column = alphabeta(mapConfig, depth, float("-inf"), float("inf"), False)[1]
            print "\nPepeto played %s"%column
        else:
            printMap(mapConfig.pmap)
            column = int(raw_input('%s\'s move (100=pass, 200=abandon) : '%(mapConfig.players[nextplayer])))
            while(column < 0 or column >= width or mapConfig.colHeads[column] >= height ):
                column = int(raw_input('BAD MOVE ! %s\'s move (100=pass, 200=abandon) : '%(mapConfig.players[nextplayer])))
        #printMap(mapConfig.pmap)
        if(column == 200):
            mapConfig.winner, mapConfig.state  = 1-nextplayer, 0
        else:
            (facw,mapConfig.winpos,mapConfig.winor) = fillAndCheckWin(mapConfig,column,nextplayer)
            if(facw == 1):
                mapConfig.winner, mapConfig.state = nextplayer, 0
            elif(lenturns == tsize - 1):            
                mapConfig.state   = 2        
            evalUp(mapConfig,column)    
            #print "after evalUp: %s"%(mapConfig.heurMap)
            mapConfig.colHeads[column] += 1
            mapConfig.turns.append(column)
    printMap(mapConfig.pmap) 
    print "\n###################################"
    print """
    Winner       : %s
    Total turns  : %s
    Final state  : %s
    WinPos       : %s:%s
    Orientation  : %s\n
    TurnsHistory : %s"""%(mapConfig.players[mapConfig.winner],len(mapConfig.turns),states[mapConfig.state],mapConfig.winpos%width,mapConfig.winpos/width,mapConfig.winor,mapConfig.turns)
                
