/*
   Created on 24 febr. 2014;
   @author: ylmkpotufe;
   */

var width   = 7;
var height  = 6;
var winlen  = 4;
var tsize   = width*height;
var visuals = ['yellow','red',''];
var timeout = 20;
var signs   = [-1,1];
var states  = ['WIN','PLAYING','DRAW'];
var depth   = 2;
var e       = null;
var p1Name  = "N/A";
var me      = 1;
var powers  = [10,110,1110]

var MapConfig = function() {
  //""" A map configuration for(p4 """;
  this.pmap       = [];
  this.colHeads   = [];
  this.turns      = [];
  //0=terminated, 1=running, 2=pat, 3=pause;
  this.state      = 1;
  this.winner     = 2;
  this.winpos     = -1;
  this.winor      = '*';
  this.players    = [p1Name,"Player2","No winner"];
  this.heuristic  = 0;
  this.heurMap    = [];
  for(var i=0;i<tsize;i++) {
    this.pmap.push(2);
    this.heurMap.push(0);
  }
  for(var i=0;i<width;i++){
    this.colHeads.push(0);
  }
}

function scoreUpHelper(wcoef,hcoef,spos,_map){
  var res     = [];
  var orient  = wcoef*width+hcoef;
  var tempos  = spos;
  var clr     = _map[spos];
  while( tempos+orient >= 0 && tempos+orient<tsize && (tempos%width+hcoef)>=0 && (tempos%width+hcoef)<width && ( _map[tempos+orient] == clr /*||  _map[tempos+orient] == 2*/)){
    res.push(tempos+orient);
    tempos = tempos+orient;
  }
  //  res.push((i<(winlen-1)));//This means no win can be achieved on this sens (direction)
  res.push(!(tempos+orient >= 0 && tempos+orient<tsize && (tempos%width+hcoef)>=0 && (tempos%width+hcoef)<width &&  _map[tempos+orient] == 2));
  return res;
}
function scoreUp(wcoef,hcoef,spos,_map){
  return len(scoreUpHelper(wcoef,hcoef,spos,_map))+len(scoreUpHelper(-wcoef,-hcoef,spos,_map))-1;
}

function evalUpHelper(spos,_map,_heurMap,_player,wcoef,hcoef){
  var p1  = scoreUpHelper(wcoef,hcoef,spos,_map);
  var wip = p1.pop(); //win impossible further on this direction
  p1      = p1.concat(scoreUpHelper(-wcoef,-hcoef,spos,_map));
  wip     = p1.pop() && wip; //win possible further on this orientation (both directions)
  //Remove old computations 
  var pw  = len(p1);
  for( elm in p1) {
    _heurMap[p1[elm]] -= Math.pow(10,pw) * signs[_player];
  }

  p1.push(spos);
  pw  = len(p1);
  if(pw>=4){ 
    for( elm in p1) {
      _heurMap[p1[elm]]= Infinity*signs[_player];//Math.pow(10,300)*signs[_player];
    }
  } else if(!wip){
    for( elm in p1) {
      _heurMap[p1[elm]]+= Math.pow(10,pw)*signs[_player];
    }
  }
  return _heurMap;
}

function setHeuristic(_mapConfig){
  var h = 0;
  for( var i=0;i<tsize;i++){
    h += _mapConfig.heurMap[i];
  }
  _mapConfig.heuristic = h;
  return h;
}

function evalUp(_mapConfig,_col){
  var spos    = getPos(_mapConfig,_col);// _mapConfig.colHeads[_col]*width+_col;
  var _player = len(_mapConfig.turns)%2;
  _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,0,1);
  _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,1,0);
  _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,1,-1);
  _mapConfig.heurMap  = evalUpHelper(spos,_mapConfig.pmap,_mapConfig.heurMap,_player,1,1);
  setHeuristic(_mapConfig);
}

function isTerminal(node){
  return (len(node.turns) == tsize);
}

function heuristic(node){
  return node.heuristic;
}

function possibleColumns(node){
  var pc = [];
  for(var i=width-1;i>=0;i--) if(node.colHeads[i] < height) pc.push(i);
  return pc;
}

function fillAndCheckWin(_mapConfig,_col,_nextplayer){
  var pos                     =  getPos(_mapConfig,_col);// _mapConfig.colHeads[_col]*width+_col;
  _mapConfig.pmap[pos]        = _nextplayer;
  if(scoreUp(0,1,pos,_mapConfig.pmap) >= winlen){
    return [1,pos,'--'];
  }
  if(scoreUp(1,0,pos,_mapConfig.pmap) >= winlen){
    return [1,pos,'|'];
  }
  if(scoreUp(1,-1,pos,_mapConfig.pmap) >= winlen){
    return [1,pos,'\\'];
  }
  if(scoreUp(1,1,pos,_mapConfig.pmap) >= winlen){
    return [1,pos,'/'];
  }
  return [0,0,'*'];
}

function addTempColumn(_mapConfig, _col){
  var lt                      = len(_mapConfig.turns);
  var nextplayer              = lt%2;
  //Sanity check avoided;
  var pos                     = getPos(_mapConfig,_col);// _mapConfig.colHeads[_col]*width+_col;
  _mapConfig.pmap[pos]        = nextplayer;
  evalUp(_mapConfig,_col);
  _mapConfig.colHeads[_col]  += 1;
  _mapConfig.turns.push(_col);
  return _mapConfig;
}

function resetTempColumn(_mapConfig, _col){
  _mapConfig.turns.pop();
  _mapConfig.colHeads[_col]  -= 1;
  var lt                      = len(_mapConfig.turns);
  var _nextplayer             = lt%2;
  var pos                     = getPos(_mapConfig,_col);// _mapConfig.colHeads[_col]*width+_col;
  _mapConfig.pmap[pos]        = 2;
  return _mapConfig;
}

function alphabeta(node, depth, alpha, beta, maximizingPlayer){
  var bestcolumn    = -1;
  var hrst          = heuristic(node);
  if( depth == 0 || isTerminal(node) || Math.abs(hrst)==Infinity) { return [hrst, bestcolumn]; }
  var pcs       = possibleColumns(node);
  if( maximizingPlayer){        
    for( k in pcs){
      var column  = pcs[k]; 
      var tmpHeurMap  = node.heurMap.slice(0);
      var tmpHeur     = node.heuristic;
      addTempColumn(node,column);
      var score           = alphabeta(node, depth - 1, alpha, beta, false)[0];
      if(score > alpha){
        alpha       = score;
        bestcolumn  = column;
      }
      resetTempColumn(node,column);
      node.heurMap    = tmpHeurMap;
      node.heuristic  = tmpHeur;
      if( beta <= alpha){ break; }
    }
    
    return [alpha, bestcolumn];
  }
  else{
    for( k in pcs){
      var column  = pcs[k]; 
      var tmpHeurMap  = node.heurMap.slice(0);
      var tmpHeur     = node.heuristic;
      addTempColumn(node,column);
      score           = alphabeta(node, depth - 1, alpha, beta, true)[0];
      if( score < beta){
        beta        = score;
        bestcolumn  = column;
      }
      resetTempColumn(node,column);
      node.heurMap    = tmpHeurMap;
      node.heuristic  = tmpHeur;
      if( beta <= alpha){ break; }
    }
    return [beta, bestcolumn];
  }
}

function len(arr){
  return arr.length;
}

function buildGrid(){
  $('#gameHeader').hide(1000);
  var table   = $("#gameGrid");
  table.html("");
  //Build heads
  var thead   = "<thead>";
  for(var i=0;i<width;i++){
    thead    += "<th  id='col_head_"+i+"' class=' radius has-tip col_head'  data-tooltip title='click to play this column' width='60' >"+
      "<span id='col_"+i+"' data-column-id='"+i+"' class='col_head small radius button' >"+(i+1)+"</span>"+
      "</th>";
  }
  thead      += "</thead>";
  table.append(thead);
  var tbody   = "<tbody>";
  //Build content
  for(var j=height-1;j>=0;j-- ){
    tbody   += "<tr>";
    for(var i=0;i<width;i++){
      tbody   += "<td height='60' class='grid_item' data-column-id='"+(i+j*width)+"' id='grid_item_"+(i+j*width)+"'>"+
        "<div class='circle '>&nbsp;</div>"+
        "</td>";
    }
    tbody   += "</tr>";
  }
  tbody   += "</tbody>";
  table.append(tbody);
}
function draw(pos, player){
  $("#grid_item_"+pos+" div").addClass(visuals[player]);
}

function clear(pos){
  $("#grid_item_"+pos+" div").removeClass(visuals[0]).removeClass(visuals[1]);;
}

function loadGame(e){
  p1Name                  = $.trim($("#p1Name").val()) || "You";
  if(document.getElementById("youStart").checked){
    me                    = 0;
  }else{
    me                    = 1;
  }

  if(document.getElementById("colorYellow").checked){
    visuals[me]           = 'yellow';
    visuals[1-me]         = 'red';
  }else{
    visuals[1-me]         = 'yellow';
    visuals[me]           = 'red';
  }
  depth                   = parseInt($("#depth").val()) + ((me==0)?1:0) ; 
  $('#history').html("");
  buildGrid();
  mapConfig               = new MapConfig();
  mapConfig.players[1-me] = "Pepeto";
  mapConfig.players[me]   = p1Name;
  $(".col_head .button").on('click',function(){
    column                = parseInt($(this).attr('data-column-id'));
    play(column,me);
  });
  getNextPlay();

  return false;

}

function getNextPlay(){
  if(mapConfig.state == 1){
    lenturns    = len(mapConfig.turns);
    nextplayer  = lenturns%2;
    authorizePlayer(nextplayer);
  }else{
    gameSummary(); 
  }
}

function gameSummary(){
  $('#info').html("<h3>Summary</h3><table cellspacing='0' cellpadding='0'>"+
      "<tr><td><b>Winner       </b>:</td><td> "+mapConfig.players[mapConfig.winner]+"<br/></td></tr>"+
      "<tr><td><b>Total turns  </b>:</td><td> "+len(mapConfig.turns)+"<br/></td></tr>"+
      "<tr><td><b>Final state  </b>:</td><td> "+states[mapConfig.state]+"<br/></td></tr>"+
      "<tr><td><b>WinPos       </b>:</td><td> "+mapConfig.winpos%width+":"+Math.ceil(mapConfig.winpos/width)+"<br/></td></tr>"+
      "<tr><td><b>Orientation  </b>:</td><td> "+mapConfig.winor+"<br/></td></tr>"+
      "</td></tr></table>");

  $(".col_head .button").hide();
  $("#back").hide();

}

function authorizePlayer(nextplayer){
  $("#info").html("Next player: <b>"+ mapConfig.players[nextplayer]+"</b>");   
  if( mapConfig.players[nextplayer] == "Pepeto"){
    $(".col_head .button").hide();
    var column      = -1;
    var depthDelta  = 0;
    while(column == -1 && depth > depthDelta){
      column  = alphabeta(mapConfig, depth-depthDelta, -Infinity, Infinity, (me==0))[1];
      depthDelta++;
    }
    if(column == -1){
      column  = possibleColumns(mapConfig)[0];
    }
    $("#info").append("<br/>Pepeto played "+column);
    play(column,nextplayer);
  }else{
    $(".col_head .button").show(300);
    if(mapConfig.turns.length > 1) $("#back").show(300); else $("#back").hide();
  }

}

function play(column,nextplayer){
  pos  = getPos(mapConfig,column); //mapConfig.colHeads[column]*width+column;
  draw(pos,nextplayer);
  $('#history').append("<tr><td><b>"+mapConfig.players[nextplayer]+"</b> </td><td>played </td><td>"+(column+1)+"</td></tr>"); 
  var res           = fillAndCheckWin(mapConfig,column,nextplayer);
  facw              = res[0];
  mapConfig.winpos  = res[1];
  mapConfig.winor   = res[2];
  if(facw == 1){
    mapConfig.winner  = nextplayer;
    mapConfig.state   = 0;
  }
  else if(lenturns == tsize - 1){
    mapConfig.state   = 2;
  }
  evalUp(mapConfig,column);
  mapConfig.colHeads[column] += 1;
  mapConfig.turns.push(column);
  if(mapConfig.colHeads[column]>=height){
    $("#col_"+column).remove();
  }
  getNextPlay();
}

function cancelLast(){
  cancelHelper();
  cancelHelper();
  col     = mapConfig.turns[mapConfig.turns.length-1];
  resetTempColumn(mapConfig,mapConfig.turns[mapConfig.turns.length-1]);
  addTempColumn(mapConfig,col); 

  $(".col_head .button").unbind('click').bind('click',function(){
    column = parseInt($(this).attr('data-column-id'));
    play(column,me);
  });
  if(mapConfig.turns.length < 3) $("#back").hide();
}

function cancelHelper(){
  var col = mapConfig.turns[mapConfig.turns.length-1];
  resetTempColumn(mapConfig,col);
  clear(getPos(mapConfig,col));
  $("#history tr").last().remove();
  $("#col_head_"+col).html("<span id='col_"+col+"' data-column-id='"+col+"' class='col_head small radius button' >"+(col+1)+"</span>");
}

function getPos(mapConfig,_col){
  return mapConfig.colHeads[_col]*width+_col;
}

$(function(){
});


